#include "Matrix3.h"


/// <summary>
/// Default Constructor
/// initializes all elements to 0 (creates a zero matrix)
/// </summary>
Matrix3::Matrix3() :
	m_A11(0.0), m_A12(0.0), m_A13(0.0),
	m_A21(0.0), m_A22(0.0), m_A23(0.0),
	m_A31(0.0), m_A32(0.0), m_A33(0.0)
{
}

/// <summary>
/// Overloaded Constructor
/// Creates a matrix using 3 vectors as rows
/// </summary>
/// <param name="row1"> 1st row as a vector </param>
/// <param name="row2"> 2nd row as a vector </param>
/// <param name="row3"> 3rd row as a vector </param>
Matrix3::Matrix3(Vector3 row1, Vector3 row2, Vector3 row3) :
	m_A11(row1.getX()), m_A12(row1.getY()), m_A13(row1.getZ()),
	m_A21(row2.getX()), m_A22(row2.getY()), m_A23(row2.getZ()),
	m_A31(row3.getX()), m_A32(row3.getY()), m_A33(row3.getZ())
{
}

/// <summary>
/// Overloaded Constructor
/// Creates a matrix with individually set components
/// </summary>
/// <param name="A11"> row 1 collumn 1 </param>
/// <param name="A12"> row 1 collumn 2 </param>
/// <param name="A13"> row 1 collumn 3 </param>
/// <param name="A21"> row 2 collumn 1 </param>
/// <param name="A22"> row 2 collumn 2 </param>
/// <param name="A23"> row 2 collumn 3 </param>
/// <param name="A31"> row 3 collumn 1 </param>
/// <param name="A32"> row 3 collumn 2 </param>
/// <param name="A33"> row 3 collumn 3 </param>
Matrix3::Matrix3(
	double A11, double A12, double A13,
	double A21, double A22, double A23,
	double A31, double A32, double A33
) :
	m_A11(A11), m_A12(A12), m_A13(A13),
	m_A21(A21), m_A22(A22), m_A23(A23),
	m_A31(A31), m_A32(A32), m_A33(A33)
{
}

/// <summary>
/// Of the given matrix where,
/// each column becomes a row,
/// each row becomes a column
/// </summary>
/// <param name="m"> given matrix </param>
/// <returns> Returns a Matrix3, as the result </returns>
Matrix3 Matrix3::transpose(const Matrix3& m)
{
	return Matrix3(m.column(0), m.column(1), m.column(2));
}

/// <summary>
/// will get the inverse of a matrix,
/// using the following formula:
/// inverse of matrix A=
/// ( 1/determinant of A ) * (row reduced matrix A)
/// row      =  [ A33 * A22 - A32 * A23, A32 * A13 - A33 * A12, A23 * A12 - A22 * A13 ]
/// reduced  =  [ A31 * A23 - A33 * A21, A33 * A11 - A31 * A13, A21 * A13 - A23 * A11 ]
/// matrix A =  [ A32 * A21 - A31 * A22, A31 * A12 - A32 * A11, A22 * A11 - A21 * A12 ]
/// 
/// if determinant = 0, than inverse does not exist
/// </summary>
/// <param name="m"> Matrix3 of which you want the inverse of </param>
/// <returns> Returns a 3x3 matrix, as the inverse </returns>
Matrix3 Matrix3::inverse(const Matrix3& m)
{
	double det = m.determinant();
	if (det == 0.0)
	{
		return Matrix3();
	}
	else
	{
		return
			Matrix3(
				m.m_A33 * m.m_A22 - m.m_A32 * m.m_A23, m.m_A32 * m.m_A13 - m.m_A33 * m.m_A12, m.m_A23 * m.m_A12 - m.m_A22 * m.m_A13,
				m.m_A31 * m.m_A23 - m.m_A33 * m.m_A21, m.m_A33 * m.m_A11 - m.m_A31 * m.m_A13, m.m_A21 * m.m_A13 - m.m_A23 * m.m_A11,
				m.m_A32 * m.m_A21 - m.m_A31 * m.m_A22, m.m_A31 * m.m_A12 - m.m_A32 * m.m_A11, m.m_A22 * m.m_A11 - m.m_A21 * m.m_A12
			) * (1 / det);
	}
}

/// <summary>
/// Creates a rotation matrix,
/// where you rotate around the passed axis
/// </summary>
/// <param name="axis"> axis that you rotate about </param>
/// <param name="angle"> angle of rotation in degrees </param>
/// <returns> Returns a rotation matrix about the passed axis with the passed angle </returns>
Matrix3 Matrix3::rotation(const Axis& axis, const double& angle)
{
	double radAngle = angle * (acos(-1) / 180);
	switch (axis)
	{
	case Matrix3::Axis::X:
		return Matrix3(
			1.0, 0.0, 0.0,
			0.0, cos(radAngle), -sin(radAngle),
			0.0, sin(radAngle), cos(radAngle)
		);
		break;
	case Matrix3::Axis::Y:
		return Matrix3(
			cos(radAngle), 0.0, -sin(radAngle),
			0.0, 1.0, 0.0,
			sin(radAngle), 0.0, cos(radAngle)
		);
		break;
	case Matrix3::Axis::Z:
		return Matrix3(
			cos(radAngle), -sin(radAngle), 0.0,
			sin(radAngle), cos(radAngle), 0.0,
			0.0, 0.0, 1.0
		);
		break;
	default:
		return Matrix3();
		break;
	}
}

/// <summary>
/// Will create a translation matrix,
/// that moves something accross the passed arguments
/// </summary>
/// <param name="x"> offset along the x-axis </param>
/// <param name="y"> offset along the y-axis </param>
/// <returns> Returns a translation matrix </returns>
Matrix3 Matrix3::translate(const double& x, const double& y)
{
	return Matrix3(
		1.0, 0.0, x,
		0.0, 1.0, y,
		0.0, 0.0, 1.0
	);
}

/// <summary>
/// Will create a Scale matrix,
/// using the formula,
/// [   x		,0		,0  ]
/// [   0		,y		,0  ]
/// [   0		,0		,z  ]
/// Note:
///     x == 2 than 2 times the x-size
/// </summary>
/// <param name="x"> scale along the x-axis </param>
/// <param name="y"> scale along the y-axis </param>
/// <param name="z"> scale along the z-axis </param>
Matrix3 Matrix3::scale(const double& x, const double& y, const double& z)
{
	return Matrix3(
		x, 0.0, 0.0,
		0.0, y, 0.0,
		0.0, 0.0, z
	);
}

/// <summary>
/// Will get the determinant of the passed matrix,
/// using the following formula:
/// determinant =
/// A11 * ( A22*A33 - A32*A23 ) - A21 * ( A33*A12 - A32*A13 ) + A31 * ( A23*A12 - A22*A13 )
/// ARW
/// A = matrix, R = row, W = column
/// Example: A13 = a matrix, row 1, column 3
/// </summary>
/// <returns> Returns a double, as the determinant </returns>
double Matrix3::determinant() const
{
	return (m_A11 * (m_A22 * m_A33 - m_A32 * m_A23) -
		m_A21 * (m_A33 * m_A12 - m_A32 * m_A13) +
		m_A31 * (m_A23 * m_A12 - m_A22 * m_A13));
}

/// <summary>
/// Will get the values of one of the rows,
/// 0 = first row, 1 = second row, 2 = last row,
/// anything else = last row,
/// </summary>
/// <param name="i"> integer to indicate what row </param>
/// <returns> Returns a Vector3, corresponding to one of the rows of the matrix </returns>
Vector3 Matrix3::row(const int& i) const
{
	switch (i)
	{
	case 0:
		return Vector3(m_A11, m_A12, m_A13);
	case 1:
		return Vector3(m_A21, m_A22, m_A23);
	case 2:
	default:
		return Vector3(m_A31, m_A32, m_A33);
	}
}

/// <summary>
/// Will get the values of each of the specified column,
/// 0 = first column, 1 = second column, 2 = last column,
/// anything else = last column,
/// </summary>
/// <param name="i"> integer to indicate what column </param>
/// <returns> returns a Vector3, corresponding to one of the columns of the matrix </returns>
Vector3 Matrix3::column(const int& i) const
{
	switch (i)
	{
	case 0:
		return Vector3(m_A11, m_A21, m_A31);
	case 1:
		return Vector3(m_A12, m_A22, m_A32);
	case 2:
	default:
		return Vector3(m_A13, m_A23, m_A33);
	}
}

/// <summary>
/// Outputs our matrix as a string,
/// in the following format,
/// (r1c1, r1c2, r1c3)
/// (r2c1, r2c2, r2c3)
/// (r3c1, r3c2, r3c3)
/// </summary>
/// <returns> Returns a format'd string </returns>
std::string Matrix3::toString() const
{
	return std::string(
		"(" + std::to_string(m_A11) + "," + std::to_string(m_A12) + "," + std::to_string(m_A13) + ")\n" +
		"(" + std::to_string(m_A21) + "," + std::to_string(m_A22) + "," + std::to_string(m_A23) + ")\n" +
		"(" + std::to_string(m_A31) + "," + std::to_string(m_A32) + "," + std::to_string(m_A33) + ")"
	);
}

/// <summary>
/// Overloaded operator+
/// adds 2 matrixs together
/// </summary>
/// <param name="lhs"> left hand side of operator </param>
/// <param name="rhs"> right hand side of operator </param>
/// <returns> returns matrix as the sum of the 2 matrixs </returns>
Matrix3 operator+(const Matrix3 & lhs, const Matrix3 & rhs)
{
	return Matrix3(lhs.row(0) + rhs.row(0), lhs.row(1) + rhs.row(1), lhs.row(2) + rhs.row(2));
}

/// <summary>
/// Overloaded operator-
/// subtracts 2 matrix's together
/// </summary>
/// <param name="lhs"> left hand side of operator </param>
/// <param name="rhs"> right hand side of operator </param>
/// <returns> returns matrix as the difference of the 2 matrix's </returns>
Matrix3 operator-(const Matrix3 & lhs, const Matrix3 & rhs)
{
	return Matrix3(lhs.row(0) - rhs.row(0), lhs.row(1) - rhs.row(1), lhs.row(2) - rhs.row(2));
}

/// <summary>
/// Overloaded operator*
/// product of a matrix with a scalar
/// </summary>
/// <param name="lhs"> left hand side of operator </param>
/// <param name="rhs"> right hand side of operator </param>
/// <returns> returns a matrix as the product of the matrix with the scalar </returns>
Matrix3 operator*(const Matrix3 & lhs, const double & rhs)
{
	return Matrix3(lhs.row(0) * rhs, lhs.row(1) * rhs, lhs.row(2) * rhs);
}

/// <summary>
/// Overloaded operator*
/// Matrix3 * Vector3,
/// using the following formula:
/// vector X component = ( A11 * x + A12 * y + A13 * z )
/// vector Y component = ( A21 * x + A22 * y + A23 * z )
/// vector Z component = ( A31 * x + A32 * y + A33 * z )
/// </summary>
/// <param name="lhs"> left hand side of operator </param>
/// <param name="rhs"> right hand side of operator </param>
/// <returns> returns vector as the transformed vector </returns>
Vector3 operator*(const Matrix3 & lhs, const Vector3 & rhs)
{
	return Vector3(rhs * lhs.row(0), rhs * lhs.row(1), rhs * lhs.row(2));
}

/// <summary>
/// Overloaded operator*
/// Matrix3 * Matrix3,
/// of the 1st matrix,
/// multiply the row into,
/// each collumn in the 2nd matrix
/// </summary>
/// <param name="lhs"> left hand side of operator </param>
/// <param name="rhs"> right hand side of operator </param>
/// <returns> returns matrix as the product between the 2 matrix's </returns>
Matrix3 operator*(const Matrix3 & lhs, const Matrix3 & rhs)
{
	return Matrix3(
		lhs.row(0) * rhs.column(0), lhs.row(0) * rhs.column(1), lhs.row(0) * rhs.column(2),
		lhs.row(1) * rhs.column(0), lhs.row(1) * rhs.column(1), lhs.row(1) * rhs.column(2),
		lhs.row(2) * rhs.column(0), lhs.row(2) * rhs.column(1), lhs.row(2) * rhs.column(2)
	);
}
