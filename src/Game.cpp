#include <Game.h>

static bool flip;

Game::Game()
	: m_window(sf::VideoMode(800u, 600u), "OpenGL Cube Vertex and Fragment Shaders")
{
}


Game::~Game(){}

void Game::run()
{
	initialize();
	
	sf::Event event;

	while (isRunning)
	{
#if (DEBUG >= 2)
		DEBUG_MSG("Game running...");
#endif
		processEvents(event);
		update();
		render();
	}

}


void Game::initialize()
{
	isRunning = true;
	GLint isCompiled = 0;
	GLint isLinked = 0;

	glewInit();

	/* Vertices counter-clockwise winding */

	// right-top-front
	m_vertices[0].coordinate[0] = HALF_WIDTH;
	m_vertices[0].coordinate[1] = HALF_WIDTH;
	m_vertices[0].coordinate[2] = HALF_WIDTH;
	m_vertices[0].color[0] = 1.0f;
	m_vertices[0].color[1] = 0.0f;
	m_vertices[0].color[2] = 1.0f;
	m_vertices[0].color[3] = 1.0f;

	// left-top-front
	m_vertices[1].coordinate[0] = -HALF_WIDTH;
	m_vertices[1].coordinate[1] = HALF_WIDTH;
	m_vertices[1].coordinate[2] = HALF_WIDTH;
	m_vertices[1].color[0] = 1.0f;
	m_vertices[1].color[1] = 0.0f;
	m_vertices[1].color[2] = 0.0f;
	m_vertices[1].color[3] = 1.0f;

	// left-bottom-front
	m_vertices[2].coordinate[0] = -HALF_WIDTH;
	m_vertices[2].coordinate[1] = -HALF_WIDTH;
	m_vertices[2].coordinate[2] = HALF_WIDTH;
	m_vertices[2].color[0] = 0.0f;
	m_vertices[2].color[1] = 0.0f;
	m_vertices[2].color[2] = 1.0f;
	m_vertices[2].color[3] = 1.0f;

	// right-bottom-front
	m_vertices[3].coordinate[0] = HALF_WIDTH;
	m_vertices[3].coordinate[1] = -HALF_WIDTH;
	m_vertices[3].coordinate[2] = HALF_WIDTH;
	m_vertices[3].color[0] = 1.0f;
	m_vertices[3].color[1] = 0.0f;
	m_vertices[3].color[2] = 0.0f;
	m_vertices[3].color[3] = 1.0f;

	// right-top-back
	m_vertices[4].coordinate[0] = HALF_WIDTH;
	m_vertices[4].coordinate[1] = HALF_WIDTH;
	m_vertices[4].coordinate[2] = -HALF_WIDTH;
	m_vertices[4].color[0] = 0.0f;
	m_vertices[4].color[1] = 1.0f;
	m_vertices[4].color[2] = 0.0f;
	m_vertices[4].color[3] = 1.0f;

	//left-top-back
	m_vertices[5].coordinate[0] = -HALF_WIDTH;
	m_vertices[5].coordinate[1] = HALF_WIDTH;
	m_vertices[5].coordinate[2] = -HALF_WIDTH;
	m_vertices[5].color[0] = 1.0f;
	m_vertices[5].color[1] = 0.0f;
	m_vertices[5].color[2] = 0.0f;
	m_vertices[5].color[3] = 1.0f;

	// left-bottom-back
	m_vertices[6].coordinate[0] = -HALF_WIDTH;
	m_vertices[6].coordinate[1] = -HALF_WIDTH;
	m_vertices[6].coordinate[2] = -HALF_WIDTH;
	m_vertices[6].color[0] = 0.0f;
	m_vertices[6].color[1] = 0.0f;
	m_vertices[6].color[2] = 1.0f;
	m_vertices[6].color[3] = 1.0f;

	// right-bottom-back
	m_vertices[7].coordinate[0] = HALF_WIDTH;
	m_vertices[7].coordinate[1] = -HALF_WIDTH;
	m_vertices[7].coordinate[2] = -HALF_WIDTH;
	m_vertices[7].color[0] = 1.0f;
	m_vertices[7].color[1] = 0.0f;
	m_vertices[7].color[2] = 0.0f;
	m_vertices[7].color[3] = 1.0f;

	/* Vertex Index initialization */

	// front face
	m_indexes[0] = 0u;   m_indexes[1] = 1u;   m_indexes[2] = 2u;
	m_indexes[3] = 3u;   m_indexes[4] = 0u;   m_indexes[5] = 2u;

	// right face
	m_indexes[6] = 0u; m_indexes[7] = 3u; m_indexes[8] = 4u;
	m_indexes[9] = 4u; m_indexes[10] = 3u; m_indexes[11] = 7u;

	// back face
	m_indexes[12] = 4u;   m_indexes[13] = 6u;   m_indexes[14] = 5u;
	m_indexes[15] = 7u;   m_indexes[16] = 6u;   m_indexes[17] = 4u;

	// left face
	m_indexes[18] = 5u;   m_indexes[19] = 6u;   m_indexes[20] = 1u;
	m_indexes[21] = 1u;   m_indexes[22] = 6u;   m_indexes[23] = 2u;

	// top face
	m_indexes[24] = 5u;   m_indexes[25] = 1u;   m_indexes[26] = 4u;
	m_indexes[27] = 0u;   m_indexes[28] = 4u;   m_indexes[29] = 1u;

	// bottom face
	m_indexes[30] = 7u;   m_indexes[31] = 2u;   m_indexes[32] = 6u;
	m_indexes[33] = 3u;   m_indexes[34] = 2u;   m_indexes[35] = 7u;

	/* Create a new VBO using VBO id */
	glGenBuffers(1, &m_vbo);

	/* Bind the VBO */
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

	/* Upload vertex data to GPU */
	glBufferData(GL_ARRAY_BUFFER, (SIZE_OF_VERTEX * NUM_VERTICES), m_vertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

	glGenBuffers(1, &m_vboIndex);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vboIndex);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, SIZE_OF_INDEXES, m_indexes, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vboIndex);

	/* Vertex Shader which would normally be loaded from an external file */
	const char* vs_src;

	/* Loading vertex shader in through a file */
	std::ifstream vsFileSource;
	vsFileSource.open(".\\resources\\shaders\\vertex.shader");
	std::string vertexSrc, line;
	while (!vsFileSource.eof())
	{
		std::getline(vsFileSource, line);
		vertexSrc.append(line + "\n");
	}
	vsFileSource.close();
	vs_src = vertexSrc.c_str();
	DEBUG_MSG("Setting Up Vertex Shader");



	vsid = glCreateShader(GL_VERTEX_SHADER); //Create Shader and set ID
	glShaderSource(vsid, 1, reinterpret_cast<const GLchar**>(&vs_src), NULL); // Set the shaders source
	glCompileShader(vsid); //Check that the shader compiles

						   //Check is Shader Compiled
	glGetShaderiv(vsid, GL_COMPILE_STATUS, &isCompiled);

	if (isCompiled == GL_TRUE) {
		DEBUG_MSG("Vertex Shader Compiled");
		isCompiled = GL_FALSE;
	}
	else
	{
		DEBUG_MSG("ERROR: Vertex Shader Compilation Error");
	}

	/* Fragment Shader which would normally be loaded from an external file */
	const char* fs_src;

	/***** Loading vertex shader in through a file *****/
	std::ifstream fsFileSource;
	fsFileSource.open(".\\resources\\shaders\\fragment.shader");
	std::string fragmentSrc;
	while (!fsFileSource.eof())
	{
		std::getline(fsFileSource, line);
		fragmentSrc.append(line + "\n");
	}
	fsFileSource.close();
	fs_src = fragmentSrc.c_str();
	DEBUG_MSG("Setting Up Fragment Shader");



	fsid = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fsid, 1, reinterpret_cast<const GLchar**>(&fs_src), NULL);
	glCompileShader(fsid);
	//Check is Shader Compiled
	glGetShaderiv(fsid, GL_COMPILE_STATUS, &isCompiled);

	if (isCompiled == GL_TRUE) {
		DEBUG_MSG("Fragment Shader Compiled");
		isCompiled = GL_FALSE;
	}
	else
	{
		DEBUG_MSG("ERROR: Fragment Shader Compilation Error");
	}

	DEBUG_MSG("Setting Up and Linking Shader");
	progID = glCreateProgram();	//Create program in GPU
	glAttachShader(progID, vsid); //Attach Vertex Shader to Program
	glAttachShader(progID, fsid); //Attach Fragment Shader to Program
	glLinkProgram(progID);

	//Check is Shader Linked
	glGetProgramiv(progID, GL_LINK_STATUS, &isLinked);

	if (isLinked == 1) {
		DEBUG_MSG("Shader Linked");
	}
	else
	{
		DEBUG_MSG("ERROR: Shader Link Error");
	}

	// Use Progam on GPU
	// https://www.opengl.org/sdk/docs/man/html/glUseProgram.xhtml
	glUseProgram(progID);

	// Find variables in the shader
	// https://www.khronos.org/opengles/sdk/docs/man/xhtml/glGetAttribLocation.xml
	positionID = glGetAttribLocation(progID, "sv_position");
	colorID = glGetAttribLocation(progID, "sv_color");


	// Setting camera up
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, m_window.getSize().x / m_window.getSize().y, 0.0, 6.0);
	glMatrixMode(GL_MODELVIEW);
	glTranslatef(0.0f, 0.0f, -3.0f);

	// activate openGL culling
	glEnable(GL_CULL_FACE);
}

void Game::processEvents(sf::Event & e)
{
	while (m_window.pollEvent(e))
	{
		switch (e.type)
		{
		case sf::Event::Closed:
			isRunning = false;
			break;
		case sf::Event::KeyPressed:
			processKeyEvents(e.key.code);
			break;
		default:
			break;
		}
	}
}

void Game::processKeyEvents(const sf::Keyboard::Key & keyPressed)
{
	const float MOVEMENT = 0.02f;
	const double ANGLE = 1.0;
	const double SCALE_UP = 1.1;
	const double SCALE_DOWN = 0.9;
	const double TRANSLATE = 0.2;

	const Quaternion TRANSFORM_Q = Quaternion(0.0, 0.0, 1.0, 0.0);

	Vector3 point = Vector3(0.0, 0.0, 0.0);

	switch (keyPressed)
	{
	case sf::Keyboard::Escape:
		isRunning = false;
		break;
		for (auto & vertex : m_vertices)
		{
			vertex.coordinate[0] -= MOVEMENT;
		}
		break;
	case sf::Keyboard::D:
		for (auto & vertex : m_vertices)
		{
			vertex.coordinate[0] += MOVEMENT;
		}
		break;
	case sf::Keyboard::W:
		for (auto & vertex : m_vertices)
		{
			vertex.coordinate[1] += MOVEMENT;
		}
		break;
	case sf::Keyboard::S:
		for (auto & vertex : m_vertices)
		{
			vertex.coordinate[1] -= MOVEMENT;
		}
		break;
	case sf::Keyboard::Up:
		m_transform = Matrix3::rotation(Matrix3::Axis::X, ANGLE);
		for (auto & vertex : m_vertices)
		{
			point = Vector3(vertex.coordinate[0], vertex.coordinate[1], vertex.coordinate[2]);
			point = m_transform * point;
			vertex.coordinate[0] = static_cast<float>(point.getX());
			vertex.coordinate[1] = static_cast<float>(point.getY());
			vertex.coordinate[2] = static_cast<float>(point.getZ());
		}
		break;
	case sf::Keyboard::Down:
		m_transform = Matrix3::rotation(Matrix3::Axis::X, -ANGLE);
		for (auto & vertex : m_vertices)
		{
			point = Vector3(vertex.coordinate[0], vertex.coordinate[1], vertex.coordinate[2]);
			point = m_transform * point;
			vertex.coordinate[0] = static_cast<float>(point.getX());
			vertex.coordinate[1] = static_cast<float>(point.getY());
			vertex.coordinate[2] = static_cast<float>(point.getZ());
		}
		break;
	case sf::Keyboard::Left:
		for (auto & vertex : m_vertices)
		{
			point = Vector3(vertex.coordinate[0], vertex.coordinate[1], vertex.coordinate[2]);
			point = TRANSFORM_Q.rotate(point, -ANGLE);
			vertex.coordinate[0] = static_cast<float>(point.getX());
			vertex.coordinate[1] = static_cast<float>(point.getY());
			vertex.coordinate[2] = static_cast<float>(point.getZ());
		}
		break;
	case sf::Keyboard::Right:
		for (auto & vertex : m_vertices)
		{
			point = Vector3(vertex.coordinate[0], vertex.coordinate[1], vertex.coordinate[2]);
			point = TRANSFORM_Q.rotate(point, ANGLE);
			vertex.coordinate[0] = static_cast<float>(point.getX());
			vertex.coordinate[1] = static_cast<float>(point.getY());
			vertex.coordinate[2] = static_cast<float>(point.getZ());
		}
		break;
	case sf::Keyboard::X:
		m_transform = Matrix3::scale(SCALE_DOWN, SCALE_DOWN, SCALE_DOWN);
		for (auto & vertex : m_vertices)
		{
			point = Vector3(vertex.coordinate[0], vertex.coordinate[1], vertex.coordinate[2]);
			point = m_transform * point;
			vertex.coordinate[0] = static_cast<float>(point.getX());
			vertex.coordinate[1] = static_cast<float>(point.getY());
			vertex.coordinate[2] = static_cast<float>(point.getZ());
		}
		break;
	case sf::Keyboard::Z:
		m_transform = Matrix3::scale(SCALE_UP, SCALE_UP, SCALE_UP);
		for (auto & vertex : m_vertices)
		{
			point = Vector3(vertex.coordinate[0], vertex.coordinate[1], vertex.coordinate[2]);
			point = m_transform * point;
			vertex.coordinate[0] = static_cast<float>(point.getX());
			vertex.coordinate[1] = static_cast<float>(point.getY());
			vertex.coordinate[2] = static_cast<float>(point.getZ());
		}
		break;
	case sf::Keyboard::Q:
		m_transform = Matrix3::translate(-TRANSLATE, 0.0);
		for (auto & vertex : m_vertices)
		{
			point = Vector3(vertex.coordinate[0], vertex.coordinate[1], vertex.coordinate[2]);
			point = m_transform * point;
			vertex.coordinate[0] = static_cast<float>(point.getX());
			vertex.coordinate[1] = static_cast<float>(point.getY());
			vertex.coordinate[2] = static_cast<float>(point.getZ());
		}
		break;
	case sf::Keyboard::E:
		m_transform = Matrix3::translate(TRANSLATE, 0.0);
		for (auto & vertex : m_vertices)
		{
			point = Vector3(vertex.coordinate[0], vertex.coordinate[1], vertex.coordinate[2]);
			point = m_transform * point;
			vertex.coordinate[0] = static_cast<float>(point.getX());
			vertex.coordinate[1] = static_cast<float>(point.getY());
			vertex.coordinate[2] = static_cast<float>(point.getZ());
		}
		break;
	default:
		break;
	}
}

void Game::update()
{
	m_elapsed = m_clock.getElapsedTime();

#if (DEBUG >= 2)
	DEBUG_MSG("Update up...");
#endif

}

void Game::render()
{

#if (DEBUG >= 2)
	DEBUG_MSG("Drawing...");
#endif
	glCullFace(GL_BACK);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vboIndex);

	/*	As the data positions will be updated by the this program on the
	CPU bind the updated data to the GPU for drawing	*/
	glBufferData(GL_ARRAY_BUFFER, (SIZE_OF_VERTEX * NUM_VERTICES), m_vertices, GL_STATIC_DRAW);

	/*	Draw Triangle from VBO	(set where to start from as VBO can contain
	model components that 'are' and 'are not' to be drawn )	*/

	// Set pointers for each parameter
	// https://www.opengl.org/sdk/docs/man4/html/glVertexAttribPointer.xhtml
	glVertexAttribPointer(positionID, 3, GL_FLOAT, GL_FALSE, SIZE_OF_VERTEX, 0);
	glVertexAttribPointer(colorID, 4, GL_FLOAT, GL_FALSE, SIZE_OF_VERTEX, 0);

	//Enable Arrays
	glEnableVertexAttribArray(positionID);
	glEnableVertexAttribArray(colorID);

	glDrawElements(GL_TRIANGLES, NUM_INDEXES, GL_UNSIGNED_BYTE, reinterpret_cast<void *>(NULL + 0));

	m_window.display();

}

void Game::unload()
{
#if (DEBUG >= 2)
	DEBUG_MSG("Cleaning up...");
#endif
	glDeleteProgram(progID);
	glDeleteBuffers(1, &m_vbo);
}