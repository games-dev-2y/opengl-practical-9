/// <summary>
/// @mainpage OpenGL Practical 9 - Shader
/// @Author Rafael Girao
/// @Version 1.0
/// @brief Program will load a shader
/// in from a text file and use it on a cube
///
/// Time Spent:
///		23rd/01/2017 13:00 120min (2hr)
///
/// Total Time Taken:
///		2hr 00min
/// </summary>

#include <Game.h>

int main(void)
{
	Game& game = Game();
	game.run();

	return EXIT_SUCCESS;
}