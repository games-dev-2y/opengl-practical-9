# README #

This is LAB 09

### What is this repository for? ###

* Lab 09 Reading in Shader from text file
* Version 1.0

### How do I get set up? ###

* clone repository
* ensure `SFML_SDK` environment variable exits
* ensure SFML Version SFML 2.3.2 Visual C++ 14 (2015) - 32-bit is installed
* [SFML-2.3.2-windows-vc14-32-bit.zip](http://www.sfml-dev.org/files/SFML-2.3.2-windows-vc14-32-bit.zip)
* Download GLEW from http://glew.sourceforge.net/index.html and obtain Binaries for  Windows 32-bit and 64-bit
* FreeGLUT could be used as an alternative http://freeglut.sourceforge.net 
* Setting an environment variable e.g. GLEW
* Follow this guide https://support.microsoft.com/en-us/kb/310519
* Alternatively `SET GLEW_SDK="C:\Users\#####\glew-1.13.0-win32\glew-1.13.0"`
* To check environment variable is set correctly open a command prompt and type `echo %GLEW_SDK%`
* Select a project default target `x86` when running executable

### Who do I talk to? ###

* [Rafael Plugge](mailto:rafael.plugge@email.com)