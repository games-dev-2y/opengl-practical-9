#ifndef _GAME
#define _GAME

// used for debug
#include <Debug.h>
// standard input/output stream
#include <iostream>
// standard file stream
#include <fstream>
// opengl wrangler library
#include <GL/glew.h>
#include <GL/wglew.h>
// SFML Window and OpenGL libraries
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>

// Project class references
#include <Vector3.h>
#include <Matrix3.h>
#include <Quaternion.h>

class Game
{
public:
	Game();
	~Game();
	void run();
private:
	sf::Window m_window;
	bool isRunning = false;
	void initialize();
	void processEvents(sf::Event &);
	void processKeyEvents(const sf::Keyboard::Key &);
	void update();
	void render();
	void unload();

	sf::Clock m_clock;
	sf::Time m_elapsed;

	float m_rotationAngle = 0.0f;

	
	static const unsigned int NUM_VERTICES = 8u;

	// Represents a openGL 3D vertex (contains coordinates and colour)
	struct Vertex
	{
	float coordinate[3];
	float color[4];
	} m_vertices[NUM_VERTICES];

	const float HALF_WIDTH = 0.5f;

	static const unsigned int NUM_INDEXES = 36u;

	GLubyte m_indexes[NUM_INDEXES];

	// storing the sizeof(Glubyte)
	static const unsigned int SIZE_OF_INDEXES = sizeof(GLubyte) * NUM_INDEXES;


	/* Variable to hold the VBO identifier and Shader Data */
	
	GLuint m_vbo = 1u; // Vertex Buffer ID
	GLuint m_vboIndex = 1u; // Vertex buffer index ID
	GLuint vsid; //Vertex Shader ID
	GLuint fsid; //Fragment Shader ID
	GLuint progID; //Program ID
	GLuint positionID; //Position ID
	GLuint colorID; // Color ID

	// storing the sizeof(Vertex)
	static const unsigned int SIZE_OF_VERTEX = sizeof(Vertex);

	Matrix3 m_transform;

	Quaternion m_transformQ;

};

#endif // !_GAME