#pragma once

#include <string>
#include <math.h>

class Vector3
{
	friend Vector3 operator+(const Vector3&, const Vector3&);
	friend Vector3 operator-(const Vector3&, const Vector3&);
	friend Vector3 operator-(const Vector3&);
	friend double operator*(const Vector3&, const Vector3&);
	friend Vector3 operator*(const Vector3&, const double&);
	friend Vector3 operator*(const Vector3&, const float&);
	friend Vector3 operator*(const Vector3&, const int&);
	friend Vector3 operator^(const Vector3&, const Vector3&);

public:
	Vector3();
	Vector3(double, double, double);

	double getX() const;
	double getY() const;
	double getZ() const;

	double length() const;
	double lengthSquare() const;
	void normalise();
	std::string toString() const;

private:
	double m_x;
	double m_y;
	double m_z;
};